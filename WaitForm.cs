﻿using System;
using System.Windows.Forms;

namespace FileDiff
{
    public partial class WaitForm : Form
    {
        public bool Cancel = false;

        public WaitForm()
        {
            InitializeComponent();
        }

        public string Title
        {
            get { return this.Text; }
            set { this.Text = value; }
        }

        public string LabelText
        {
            get { return label1.Text; }
            set { label1.Text = value; }
        }

        public bool CancelButtonEnabled
        {
            get { return button1.Enabled; }
            set { button1.Enabled = value; }
        }

        public int Max
        {
            get { return progressBar1.Maximum; }
            set
            {
                progressBar1.Maximum = value;
                toolStripStatusLabel1.Text = string.Format("{0} / {1}", progressBar1.Value, progressBar1.Maximum);
            }
        }

        public void SetMax(int max)
        {
            this.Invoke(new Action(() =>
                {
                    this.progressBar1.Maximum = max;
                    toolStripStatusLabel1.Text = string.Format("{0} / {1}", progressBar1.Value, progressBar1.Maximum);
                }));
        }

        public int Min
        {
            get { return progressBar1.Minimum; }
            set { progressBar1.Minimum = value; }
        }

        public int Value
        {
            get { return progressBar1.Value; }
            set
            {
                progressBar1.Value = value;
                toolStripStatusLabel1.Text = string.Format("{0} / {1}", progressBar1.Value, progressBar1.Maximum);
            }
        }

        public int InvokeValue
        {
            get { return progressBar1.Value; }
            set
            {
                this.Invoke(new Action<int>(v => progressBar1.Value = v), value);
                this.Invoke(new Action(() => toolStripStatusLabel1.Text = string.Format("{0} / {1}", progressBar1.Value, progressBar1.Maximum)));
            }
        }

        public ProgressBarStyle BarStyle
        {
            get { return progressBar1.Style; }
            set
            {
                if (!this.Visible) progressBar1.Style = value;
                else Invoke(new Action(() => progressBar1.Style = value));
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (button1.Text == "キャンセル")
                Cancel = true;
            else
                this.Close();
        }

        public void LogPrint(string str)
        {
            Invoke(new Action(() =>
            {
                this.textBox1.AppendText(DateTime.Now.ToString() + "  " + str + "\r\n");
            }));
        }

        public void LogSave(string FileName)
        {
            Invoke(new Action(() =>
            {
                using (var sw = new System.IO.StreamWriter(FileName, false, System.Text.Encoding.UTF8))
                {
                    sw.Write(textBox1.Text);
                }
            }));
        }

        public void ShowDialogOtherTask()
        {
            System.Threading.Tasks.Task.Factory.StartNew(() => ShowDialog());
            while (!Visible) System.Threading.Thread.Sleep(10);
        }


        protected override void Dispose(bool disposing)
        {
            if (IsHandleCreated)
            {
                Invoke(new Action(() =>
                {
                    if (disposing && (components != null))
                    {
                        components.Dispose();
                    }
                    base.Dispose(disposing);
                }));
            }
            else
            {
                if (disposing && (components != null))
                {
                    components.Dispose();
                }
                base.Dispose(disposing);
            }
        }
    }
}
