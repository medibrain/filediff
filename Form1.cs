﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FileDiff
{
    public partial class frmMain : Form
    {
        //フォルダが来た場合のファイルリスト
        List<string> lstFiles1 = new List<string>();
        List<string> lstFiles2 = new List<string>();


        //どちらかにフォルダが来た場合、そのパスを持っておく
        string strFolderPath1 = string.Empty;
        string strFolderPath2 = string.Empty;



        //改行コード抜いたリスト。これを以て文字列比較し、lst3compareに差分を格納
        List<string> lst1compare = new List<string>();
        List<string> lst2compare = new List<string>();
        List<string> lst3compare = new List<string>();



        public frmMain()
        {
            InitializeComponent();
        }

     

        private void DragDropOnTextBox(object sender, DragEventArgs e)
        {
            string[] arrstr = (string[])e.Data.GetData(DataFormats.FileDrop, false);
            TextBox txt = (TextBox)sender;
            List<string> lstFilestmp = new List<string>();


            if (txt.Name == "textBox1") lstFilestmp = lstFiles1;//lstFiles1.Clear();
            if (txt.Name == "textBox2") lstFilestmp = lstFiles2;//lstFiles2.Clear();

            lstFilestmp.Clear();
            WaitForm wf = new WaitForm();
            try
            {
                string strPath = arrstr[0];
                

                if (System.IO.Directory.Exists(strPath))
                {

                    
                    wf.ShowDialogOtherTask();
                    wf.LogPrint("ファイル取得中");
                    lstFilestmp =
                        System.IO.Directory.GetFiles(strPath, "*", System.IO.SearchOption.AllDirectories)
                        .Where(s=>!s.Contains("Thumbs.db")||!s.Contains("thumbs.db")).ToList<string>();
                    

                    System.Text.StringBuilder sb = new StringBuilder();

                    foreach (string strfile in lstFilestmp) sb.AppendLine(System.IO.Path.GetFileName(strfile));
                   

                    //lstFiles1 = System.IO.Directory.GetFiles(strPath, "*", System.IO.SearchOption.AllDirectories).ToList<string>();
                    //System.Text.StringBuilder sb = new StringBuilder();
                    //foreach (string strfile in lstFiles1) sb.AppendLine(System.IO.Path.GetFileName(strfile));

                    txt.Text = sb.ToString();


                    //最後の改行を削除
                    if (txt.Text.Substring(txt.Text.Length - 1) == "\n" ||
                        txt.Text.Substring(txt.Text.Length - 1) == Environment.NewLine) txt.Text = txt.Text.Substring(0, txt.Text.Length - 1);


                    //情報表示
                    if (txt.Name == "textBox1")
                    {
                        labelCnt1.Text = $"件数:{lstFilestmp.Count}";
                        strFolderPath1 = System.IO.Path.GetDirectoryName(lstFilestmp[0]);
                        labelPath1.Text = strFolderPath1;
                    }
                    if (txt.Name == "textBox2")
                    {
                        labelCnt2.Text = $"件数:{lstFilestmp.Count}";
                        strFolderPath2 = System.IO.Path.GetDirectoryName(lstFilestmp[0]);
                        labelPath2.Text = strFolderPath2;
                    }

                }


       

            }
            catch (Exception ex)
            {
                MessageBox.Show($"{System.Reflection.MethodBase.GetCurrentMethod().Name}\r\n{ex.Message}");

            }
            finally
            {
                wf.Dispose();
            }
        }

     

        private void textBox1_DragEnter(object sender, DragEventArgs e)
        {
            if (!e.Data.GetDataPresent(DataFormats.FileDrop)) return;
            e.Effect = DragDropEffects.Copy;
        }



        private void buttonDiff_Click(object sender, EventArgs e)
        {

            if (textBox1.Text == string.Empty) return;
            if (textBox2.Text == string.Empty) return;

            WaitForm wf = new WaitForm();
            wf.ShowDialogOtherTask();
            
            textBox3.Text = string.Empty;

            

            //テキストボックスからの文字を格納する
            List<string> lst1 = new List<string>();
            List<string> lst2 = new List<string>();

            lst1 = textBox1.Text.Split('\n').ToList<string>();
            lst2 = textBox2.Text.Split('\n').ToList<string>();

            lst1compare.Clear();
            lst2compare.Clear();
            lst3compare.Clear();
            //改行コード抜く作業
            foreach (string s in lst1)
            {
                lst1compare.Add(s.Replace("\r", "").Replace("\n", ""));
                lst3compare.Add(s.Replace("\r", "").Replace("\n", ""));
            }
            foreach (string s in lst2) lst2compare.Add(s.Replace("\r", "").Replace("\n", ""));

            buttonMove.Enabled = false;
            buttonCompress.Enabled = false;
                
            bool flgDel = false;
            try
            {
               

                //lst1compareとlst2compareを全ループして両方にある文字列をlst3compareから削除。最後にlst3compareを出すのでそっちだけの削除
                foreach (string s1 in lst1compare)
                {
                    wf.LogPrint($"{s1}比較中");

                    foreach (string s2 in lst2compare)
                    {
                        if (CommonTool.WaitFormCancelProcess(wf)) return;
                        
                        if(s1.Contains(s2))
                        //if (lst2compare.Contains(s1))
                        {
                            lst3compare.Remove(s1);
                            flgDel = true;
                            break;
                        }
                    }
                }

                if (!flgDel || lst1compare.Equals(lst3compare)) textBox3.Text="同じ項目はありませんでした";
                else
                {
                    System.Text.StringBuilder sb = new StringBuilder();
                    foreach (string s in lst3compare) sb.AppendLine($"{s}");

                    textBox3.Text = sb.ToString();
                    labelCnt3.Text = $"件数：{lst3compare.Count}";
                }

                if (lst3compare.Count > 0)
                {
                    buttonMove.Enabled = true;
                    buttonCompress.Enabled = true;
                }
                else
                {

                    buttonMove.Enabled = false;
                    buttonCompress.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                buttonMove.Enabled = false;
                buttonCompress.Enabled = false;
                MessageBox.Show($"{System.Reflection.MethodBase.GetCurrentMethod().Name}\r\n{ex.Message}");
            }
            finally
            {
                wf.Dispose();
            }
        }

        private void buttonZip_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("作業開始します。宜しいですか？", Application.ProductName, 
                MessageBoxButtons.YesNo,MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No) return;

            WaitForm wf = new WaitForm();
            wf.ShowDialogOtherTask();
            System.Text.StringBuilder sb = new StringBuilder();
            List<string> lst = getfilepaths();
            foreach (string s in lst) sb.AppendFormat($"\"{strFolderPath1}\\{s}\" ");

            archive($"{strFolderPath1}\\archive",sb.ToString(),wf);


            System.Diagnostics.Process.Start(strFolderPath1);
            
            

            wf.Dispose();
           
        }


        private List<string> getfilepaths()
        {
            
            List<string> lstCompress = new List<string>();

            foreach (string s in lst3compare)
            {
                if (s != string.Empty && !lstCompress.Contains(s)) lstCompress.Add(s);
            }

            return lstCompress;
        }


        private bool archive(string strArcFileName,string strFiles,WaitForm wf)
        {

            //Task.Run(() =>
            //{
            try
            {
                wf.LogPrint("圧縮中");
                System.Diagnostics.ProcessStartInfo si = new System.Diagnostics.ProcessStartInfo();
                si.FileName = $"{Application.StartupPath}\\7za.exe";
                si.Arguments = $" a -tzip \"{strArcFileName}.zip\" {strFiles} -mx3 -sdel -o\"{strFolderPath1}\" -stm2 ";

                System.Diagnostics.Process p = new System.Diagnostics.Process();

                si.UseShellExecute = false;
                si.CreateNoWindow = true;
                p.StartInfo = si;
                p.Start();
                p.WaitForExit();
                wf.LogPrint("終了");
                return true;
            }catch(Exception ex)
            {
                MessageBox.Show($"{System.Reflection.MethodBase.GetCurrentMethod().Name}\r\n{ex.Message}");
                return false;
            }
            //  });
        }

        private void buttonmove_Click(object sender, EventArgs e)
        {
            OpenDirectoryDiarog dlg = new OpenDirectoryDiarog();
            dlg.ShowDialog();
            string strdir = dlg.Name;
            if (strdir == string.Empty) return;

            WaitForm wf = new WaitForm();
            wf.ShowDialogOtherTask();
            List<string> lst=getfilepaths();
            try
            {
                foreach (string s in lst)
                {
                    string origfilename = $"{strFolderPath1}\\{s}";
                    string destfilename = $"{strdir}\\{System.IO.Path.GetFileName(s)}";
                    System.IO.File.Move(origfilename, destfilename);
                    wf.LogPrint($"ファイル移動中 {origfilename} {destfilename}");
                }
                System.Diagnostics.Process.Start(strdir);
            }
            catch (Exception ex)
            {
                MessageBox.Show($"{System.Reflection.MethodBase.GetCurrentMethod().Name}\r\n{ex.Message}");
            }
            finally
            {
                wf.Dispose();
                
            }

        }

        private void dispLineCount(object sender, KeyEventArgs e)
        {
            TextBox txt=(TextBox)sender;
            if (txt.Text == string.Empty) return;

            if (txt.Text.Substring(txt.Text.Length-1) == "\n" ||
                txt.Text.Substring(txt.Text.Length-1) == Environment.NewLine) txt.Text = txt.Text.Substring(0,txt.Text.Length - 1);


            if (txt.Name == "textBox1") labelCnt1.Text = $"行数：{textBox1.Text.Split('\n').Length}";
            if (txt.Name == "textBox2") labelCnt2.Text = $"行数：{textBox2.Text.Split('\n').Length}";

        }

    

        private void checkBoxExt1_CheckedChanged(object sender, EventArgs e)
        {
            
        }
    }
}
