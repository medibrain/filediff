﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.WindowsAPICodePack.Dialogs;

namespace FileDiff
{
    

    public class CommonTool
    {
        public static bool WaitFormCancelProcess(WaitForm wf)
        {
            if (wf.Cancel)
            {
                if (System.Windows.Forms.MessageBox.Show("中止しますか？", Application.ProductName,
                   System.Windows.Forms.MessageBoxButtons.YesNo,
                   System.Windows.Forms.MessageBoxIcon.Question)
                    == System.Windows.Forms.DialogResult.Yes)
                {                    
                    return true;
                }
                else
                {
                    wf.Cancel = false;
                    return false;
                }

            }
            else
            {
                return false;
            }
        }

    }


    /// <summary>
    /// フォルダ指定ダイアログ
    /// </summary>
    public class OpenDirectoryDiarog
    {
        /// <summary>
        /// フォルダ名
        /// </summary>
        public string Name { get; private set; } = string.Empty;

        public DialogResult ShowDialog()
        {
            using (var f = new CommonOpenFileDialog())
            {
                // フォルダーを開く設定に
                f.IsFolderPicker = true;

                // 読み取り専用フォルダ/コントロールパネルは開かない
                f.EnsureReadOnly = false;
                f.AllowNonFileSystemItems = false;
                var ptr = Form.ActiveForm?.Handle ?? IntPtr.Zero;
                var res = (ptr == IntPtr.Zero) ? f.ShowDialog() : f.ShowDialog(ownerWindowHandle: ptr);

                if (res == CommonFileDialogResult.Ok)
                {
                    Name = f.FileName;
                    return DialogResult.OK;
                }
                else
                {
                    return DialogResult.Cancel;
                }
            }
        }
    }



}

